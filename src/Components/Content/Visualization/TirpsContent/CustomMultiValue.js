import React from "react";
export default function CustomMultiValue(props) {
    const { getValue, data } = props;
  
    const selectedOptions = getValue();
    const currentOptionIdx = selectedOptions.findIndex(
      (option) => option.value === data.value
    );
      return (
      <span>
        {selectedOptions.find((obj) => obj.value == "") && data.label == "All" ? data.label :
       selectedOptions.find((obj) => obj.value == "") && data.label != "All" ? "" : data.label  }
        {currentOptionIdx === selectedOptions.length - 1 ? "" : 
        selectedOptions.find((obj) => obj.value == "") ? "" : ", "}
      </span>
    );
  }
  
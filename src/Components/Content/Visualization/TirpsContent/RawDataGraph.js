import React, { Component} from 'react';
import Highcharts, { objectEach } from "highcharts"
import { useEffect, useState } from 'react';
import HighchartsReact from 'highcharts-react-official';
import { CircularProgress } from '@material-ui/core';
require("highcharts/highcharts-more")(Highcharts)



// const options = {
//     chart: {
//         type: 'xrange'
//     },
//     title: {
//         text: 'Highcharts X-range'
//     },
//     accessibility: {
//         point: {
//             descriptionFormatter: function (point) {
//                 var ix = point.index + 1,
//                     category = point.yCategory,
//                     from = new Date(point.x),
//                     to = new Date(point.x2);
//                 return ix + '. ' + category + ', ' + from.toDateString() +
//                     ' to ' + to.toDateString() + '.';
//             }
//         }
//     },
//     xAxis: {
//         type: 'datetime'
//     },
//     yAxis: {
//         title: {
//             text: ''
//         },
//         categories: ['Prototyping', 'Development', 'Testing'],
//         reversed: true
//     },
//     series: [{
//         name: 'Project 1',
//         // pointPadding: 0,
//         // groupPadding: 0,
//         borderColor: 'gray',
//         pointWidth: 20,
//         data: [{
//             x: Date.UTC(2014, 10, 21),
//             x2: Date.UTC(2014, 11, 2),
//             y: 0,
//         }, {
//             x: Date.UTC(2014, 11, 2),
//             x2: Date.UTC(2014, 11, 5),
//             y: 1
//         }, {
//             x: Date.UTC(2014, 11, 8),
//             x2: Date.UTC(2014, 11, 9),
//             y: 2
//         }, {
//             x: Date.UTC(2014, 11, 9),
//             x2: Date.UTC(2014, 11, 19),
//             y: 1
//         }, {
//             x: Date.UTC(2014, 11, 10),
//             x2: Date.UTC(2014, 11, 23),
//             y: 2
//         }],
//         dataLabels: {
//             enabled: true
//         }
//     },{
//         name: 'Project 2',
//         pointWidth: 20,
//         pointHight:1,
//         type: "scatter",
//         data: [[
//             Date.UTC(2014, 10, 21),
//             0,

//         ], [
//             Date.UTC(2014, 10, 27),
//             1,

//         ],[
//             Date.UTC(2014, 11, 6),
//             2,

//         ]],
//         dataLabels: {
//             enabled: true
//         }
//     }]
//     }

const RawDataGraphNew = (props) => {
    const [options, setOptions] = useState({});
    const [rawData, setRawdata] = useState([]);
    const [descriteData, setDescritedata] = useState([]);
    const [range, setRange] = useState([]);
    const [bins, setBins] = useState({});
    const [force,forceUpdate] = useState(0);
    const [show,setShow] = useState(true);
    const [color, setColor] = useState("Disable raw data")

    const createOptions = (series, name) => {
        let options = {
            chart: {
                type: 'scatter',
                height: 175,
                // zoomType: "xy"
                // events: {
                //     load() {
                //       this.showHideFlag = true;
                //     }
                //   }
                
            },
            legend: {
                align: "right",
                layout: 'vertical',
                verticalAlign: 'middle',
                width: '10%',
                floating: false,
                itemMarginTop: 5,
                // color: "red"
            },
            accessibility: {
                // point: {
                //     descriptionFormatter: function (point) {
                //         var ix = point.index + 1,
                //             category = point.yCategory,
                //             from = new Date(point.x),
                //             to = new Date(point.x2);
                //         return ix + '. ' + category + ', ' + from.toDateString() +
                //             ' to ' + to.toDateString() + '.';
                //     }
                // }
            },
            plotOptions:{
                series:{
                    events: {
                        legendItemClick: function (){
                            if (this.name === 'See raw data') {

                                this.legendItem.label.styles.color = "#cccccc"
                                //console.log(this.legendItem.label.styles.color)
                                setShow(!show)
                                setColor("Disable raw data")

                            }
                            if (this.name === 'Disable raw data'){
                                setShow(!show)
                                setColor("See raw data")
                            }
                            // chart.showHideFlag = !chart.showHideFlag
                            
                            // console.log(chart.showHideFlag)
                            return false
                        },
                        showCheckbox: true
                    },
                },
                arearange:{
                    allowPointSelect: false,
                    enableMouseTracking: false,  
                }
            },
            xAxis: {
                min: range[0],
                max: range[1]
                // title: {
                //     text: 'Time Stamp'
                // }
            },
            yAxis: [
            {
                title: {
                    text: name
                }
                // ,
                // categories: ["low","high"]
            }],
            title: {
                text: ""
            },
            series: [{id: 'Raw Data',
                name: color,
                type: "scatter",
                data: rawData,
                color: 'grey',
                zIndex: 1,
                dataLabels: {
                    enabled: show
                },
                showInLegend: true
            },
            ...series]
            }
            return options

        }

    function size_dict(d){
        let c=0; 
        for (let i in d) ++c; 
        return c}
    
    useEffect(() => {
        //
        setRawdata(props.rawData[props.symbol_id])
        setRange(props.range)
        setBins(props.binValues)
        let symbols = []
        let counter = -1
        let range_series = []
        let name = ""
        for (const [key,value] of Object.entries(props.binValues)){
            name = key.split(".")[0]
            break
        }
        if (size_dict(options) != 0){
            let colors = ['#2b908f','#f45b5b','#90ed7d','#f7a35c', '#8085e9']
        for (const [key,value] of Object.entries(props.descriteData)){
            let shown_once = false
            counter+=1
            let descrite_daataa = value.map((iter) => {
                // if (bins[key]!= undefined){
                // if (shown_once == true){
                //     range_series.push({name: key + " [" + bins[key][0] + ":" + bins[key][1] + "]",type: 'arearange', data:[[parseInt(iter[0]),range[0],range[1]],[parseInt(iter[1]),range[0],range[1]]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: false, zIndex: 0, linkedTo: key})
                // }
                // else{
                //     range_series.push({id:key, name: key + " [" + bins[key][0] + ":" + bins[key][1] + "]",type: 'arearange', data:[[parseInt(iter[0]),range[0],range[1]],[parseInt(iter[1]),range[0],range[1]]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0})
                // }
                // if (shown_once == false){
                //     shown_once= true
                // }
                // }
                if (bins[key]!= undefined){
                    if (shown_once == true){
                        //console.log(bins[key])
                        if (bins[key][0] == "-inf"){
                            range_series.push({name: key + " [" + bins[key][0] + ":" + parseFloat(bins[key][1]).toFixed(2) + "]",type: 'arearange', data:[[parseInt(iter[0]),range[2] - 1,parseFloat(bins[key][1])],[parseInt(iter[1]),range[2] - 1,parseFloat(bins[key][1])]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: false, zIndex: 0, linkedTo: key})
                            if (iter[0] == props.highlight[0] && iter[1] == props.highlight[1]){
                                range_series.pop()
                                range_series.push({name: key + " [" + bins[key][0] + ":" + parseFloat(bins[key][1]).toFixed(2) + "]",type: 'arearange', data:[[parseInt(iter[0]),range[2] - 1,parseFloat(bins[key][1])],[parseInt(iter[1]),range[2] - 1,parseFloat(bins[key][1])]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: false, zIndex: 0, linkedTo: key, lineColor: "#303030", lineWidth: 8})

                            }
                        }
                        else if (bins[key][1] == "inf"){
                            range_series.push({name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + bins[key][1] + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),range[3] + 1],[parseInt(iter[1]),parseFloat(bins[key][0]),range[3]+1]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: false, zIndex: 0, linkedTo: key})
                            if (iter[0] == props.highlight[0] && iter[1] == props.highlight[1]){
                                range_series.pop()
                                range_series.push({name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + bins[key][1] + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),range[3] + 1],[parseInt(iter[1]),parseFloat(bins[key][0]),range[3]+1]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: false, zIndex: 0, linkedTo: key, lineColor: "#303030", lineWidth: 8})

                            }
                        }
                        else {
                            range_series.push({name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + parseFloat(bins[key][1]).toFixed(2) + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),parseFloat(bins[key][1])],[parseInt(iter[1]),parseFloat(bins[key][0]),parseFloat(bins[key][1])]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: false, zIndex: 0, linkedTo: key})
                            if (iter[0] == props.highlight[0] && iter[1] == props.highlight[1]){
                                range_series.pop()
                                range_series.push({name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + parseFloat(bins[key][1]).toFixed(2) + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),parseFloat(bins[key][1])],[parseInt(iter[1]),parseFloat(bins[key][0]),parseFloat(bins[key][1])]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: false, zIndex: 0, linkedTo: key, lineColor: "#303030", lineWidth: 8})

                            }
                        }  
                    }
                    else{
                        if (bins[key][0] == "-inf"){
                            range_series.push({id: key, name: key + " [" + bins[key][0] + ":" + parseFloat(bins[key][1]).toFixed(2) + "]",type: 'arearange', data:[[parseInt(iter[0]),range[2] - 1,parseFloat(bins[key][1])],[parseInt(iter[1]),range[2] - 1,parseFloat(bins[key][1])]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0})
                            if (iter[0] == props.highlight[0] && iter[1] == props.highlight[1]){
                                range_series.pop()
                                range_series.push({id: key, name: key + " [" + bins[key][0] + ":" + parseFloat(bins[key][1]).toFixed(2) + "]",type: 'arearange', data:[[parseInt(iter[0]),range[2] - 1,parseFloat(bins[key][1])],[parseInt(iter[1]),range[2] - 1,parseFloat(bins[key][1])]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0, lineColor: "#303030", lineWidth: 8})
                            }
                        }
                        else if (bins[key][1] == "inf"){
                            range_series.push({id: key, name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + bins[key][1] + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),range[3] + 1],[parseInt(iter[1]),parseFloat(bins[key][0]),range[3]+1]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0})
                            if (iter[0] == props.highlight[0] && iter[1] == props.highlight[1]){
                                range_series.pop()
                                range_series.push({id: key, name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + bins[key][1] + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),range[3] + 1],[parseInt(iter[1]),parseFloat(bins[key][0]),range[3]+1]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0, lineColor: "#303030", lineWidth: 8})

                            }
                        }
                        else {
                            range_series.push({id: key, name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + parseFloat(bins[key][1]).toFixed(2) + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),parseFloat(bins[key][1])],[parseInt(iter[1]),parseFloat(bins[key][0]),parseFloat(bins[key][1])]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0})
                            if (iter[0] == props.highlight[0] && iter[1] == props.highlight[1]){
                                range_series.pop()
                                range_series.push({id: key, name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + parseFloat(bins[key][1]).toFixed(2) + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),parseFloat(bins[key][1])],[parseInt(iter[1]),parseFloat(bins[key][0]),parseFloat(bins[key][1])]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0, lineColor: "#303030", lineWidth: 8})

                            }
                        }
                    }
                    if (shown_once == false){
                            shown_once= true
                        }
                }
                // else{
                //     console.log("wtf")
                //     range_series.push({name:key + " [" + props.binValues[key][0] + ":" + props.binValues[key][1] + "]",type: 'arearange',color:colors[counter], visible:false, zIndex: 0})
                // }
                // else{
                //     range_series.push({name:key + " [" + bins[key][0] + ":" + bins[key][1] + "]"})
                // }
                return {x:iter[0],x2:iter[1], y:0, color:colors[counter]}
            })
            if (value.length == 0){
                if (props.binValues[key][0] == "-inf"){
                    range_series.push({id: key, name: key + " [" + props.binValues[key][0] + ":" + parseFloat(props.binValues[key][1]).toFixed(2) + "]",type: 'arearange', color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0})
                }
                else if (props.binValues[key][1] == "inf"){
                    range_series.push({id: key, name: key + " [" + parseFloat(props.binValues[key][0]).toFixed(2) + ":" + props.binValues[key][1] + "]",type: 'arearange', color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0})
                }
                else {
                    range_series.push({id: key, name: key + " [" + parseFloat(props.binValues[key][0]).toFixed(2) + ":" + parseFloat(props.binValues[key][1]).toFixed(2) + "]",type: 'arearange', color:colors[counter], dataLabels: {enabled: false}, showInLegend: true, zIndex: 0})
                }
                //range_series.push({name:key + " [" + parseFloat(props.binValues[0]) + ":" + parseFloat(props.binValues[1]) + "]", data:[]})
            }
            
            symbols.push(...descrite_daataa)
            
            
		}

        setDescritedata(symbols)
    }
        let series = [
        // ,{
        //     name: 'Intervals',
        //     pointPadding: 0,
        //     groupPadding: 0,
        //     borderColor: 'gray',
        //     pointWidth: 20,
        //     type: 'xrange',
        //     grouping: true,
        //     data: descriteData,
        //     dataLabels: {
        //         enabled: true
        //     }
        // }
        ]
        //console.log(range_series)
        //console.log(options)
        // if (options == {}){
        //     console.log("first timeeeeeeeeeeee")
        //     series.push(...range_series)
        // }
        // else{
        //     range_series = range_series.slice(1)
        //     series.push(...range_series)
        // }
        
        series.push(...range_series)
        let res = createOptions(series,name)
        setOptions(res)

        
        //let colors = ['#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1']
        
        
	}, [rawData,show,color]);

    if (options == []){
        return (
            <CircularProgress style={{ color: 'purple', marginLeft: '45%', marginTop: '20%', width: 75 }}/>
        )
    }
    return (
            <HighchartsReact highcharts={Highcharts} options= {options}></HighchartsReact>
    )
}

class RawDataGraph extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id : props.symbol_id,
            options: {}
        }
    }
    
    componentDidMount(){
        this.createOptions(this.props.rawData)
    }

    createOptions = (data) => {
        let dat = [[]]
        if (this.props.rawData && this.props.rawData){
            let dat = data[this.state.id]
            let options = {
                chart: {
                    type: 'scatter'
                },
                accessibility: {
                    point: {
                        // descriptionFormatter: function (point) {
                        //     var ix = point.index + 1,
                        //         category = point.yCategory,
                        //         from = new Date(point.x),
                        //         to = new Date(point.x2);
                        //     return ix + '. ' + category + ', ' + from.toDateString() +
                        //         ' to ' + to.toDateString() + '.';
                        // }
                    }
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: [{
                    title: {
                        text: 'categories'
                    },
                }],
                series: [{
                    name: 'Project 1',
                    pointPadding: 0,
                    groupPadding: 0,
                    borderColor: 'gray',
                    pointWidth: 20,
                    type: 'xrange',
                    grouping: true,
                    data: [{
                        x: Date.UTC(2014, 10, 21),
                        x2: Date.UTC(2014, 11, 2),
                        y: 0,
                    }, {
                        x: Date.UTC(2014, 11, 2),
                        x2: Date.UTC(2014, 11, 5),
                        y: 1
                    }, {
                        x: Date.UTC(2014, 11, 8),
                        x2: Date.UTC(2014, 11, 9),
                        y: 2
                    }, {
                        x: Date.UTC(2014, 11, 9),
                        x2: Date.UTC(2014, 11, 19),
                        y: 1
                    }, {
                        x: Date.UTC(2014, 11, 10),
                        x2: Date.UTC(2014, 11, 23),
                        y: 1
                    }],
                    dataLabels: {
                        enabled: true
                    }
                },{
                    name: 'Project 2',
                    type: "scatter",
                    data: data[this.state.id].map((x_y) => {
                        return x_y
                    }),
                    dataLabels: {
                        enabled: true
                    }
                }]
                }
                this.setState(() => ({
                    options: options
                }));
        }
        
        
            
    }
    
	render() {
        //console.log(this.props.rawData)
        // console.log(this.props.symbol_id)
        
		return (
                <HighchartsReact highcharts={Highcharts} options= {this.state.options}></HighchartsReact>
		);
	}
}


const RawDataGraphNew2 = (props) => {
    const [options, setOptions] = useState({});
    const [rawData, setRawdata] = useState([]);
    const [descriteData, setDescritedata] = useState([]);
    const [range, setRange] = useState([]);
    const [bins, setBins] = useState({});
    const [force,forceUpdate] = useState(0);
    const [show,setShow] = useState(true);
    const [color, setColor] = useState("See raw data")

    const createOptions = (series, name) => {
        if (rawData== undefined){
            let options = {
                chart: {
                    type: 'columnrange',
                    height: 125,
                    inverted: true,
                    // zooming: "xy"
                    // events: {
                    //     load() {
                    //       this.showHideFlag = true;
                    //     }
                    //   }
                    
                },
                legend: {
                    align: "right",
                    layout: 'vertical',
                    verticalAlign: 'top',
                    width: '10%',
                    floating: true,
                    itemMarginTop: 5,
                    // color: "red"
                },
                accessibility: {
                    // point: {
                    //     descriptionFormatter: function (point) {
                    //         var ix = point.index + 1,
                    //             category = point.yCategory,
                    //             from = new Date(point.x),
                    //             to = new Date(point.x2);
                    //         return ix + '. ' + category + ', ' + from.toDateString() +
                    //             ' to ' + to.toDateString() + '.';
                    //     }
                    // }
                },
                plotOptions:{
                    series:{
                        events: {
                            legendItemClick: function (){
                                if (this.name === 'See raw data') {
    
                                    this.legendItem.label.styles.color = "#cccccc"
                                    //console.log(this.legendItem.label.styles.color)
                                    setShow(!show)
                                    setColor("Disable raw data")
    
                                }
                                if (this.name === 'Disable raw data'){
                                    setShow(!show)
                                    setColor("See raw data")
                                }
                                // chart.showHideFlag = !chart.showHideFlag
                                
                                // console.log(chart.showHideFlag)
                                return false
                            },
                            showCheckbox: true
                        },
                    },
                    columnrange: {
                        dataLabels: {
                            enabled: false,
                            // format: '{y}°C'
                        },
                        grouping: false,
                        enableMouseTracking: false,  
                    },
                    arearange:{
                        allowPointSelect: false,
                        enableMouseTracking: false,  
                    }
                },
                xAxis: {
                    // min: range[0],
                    // max: range[1]
                    title: {
                        text: name
                    },
                    categories: Object.entries(bins).map(([key, value])=> {return key}),
                    reversed: false
                },
                yAxis: [
                {
                    title: {
                        text: ""
                    },
                    min: range[0],
                    max: range[1]
                    // ,
                    // categories: ["low","high"]
                }],
                title: {
                    text: ""
                },
                series: [{id: 'Raw Data',
                    name: color,
                    type: "scatter",
                    data: rawData,
                    color: 'black',
                    zIndex: 1,
                    dataLabels: {
                        enabled: !show
                    },
                    showInLegend: true,
                    inverted: false,
                    tooltip: {
                        pointFormat: 'Time: {point.y}<br>Type:{point.x}<br> Value: {point.real_value}',
                    }
                },
                ...series]
                }
                return options
        }
        let rawData_divided = rawData.sort((a,b) => {
            if (a[0] < b[0]) {
                return -1;
              }
              if (a[0] > b[0]) {
                return 1;
              }
            
              // names must be equal
              return 0;}).map((val)=>{
            let index = 0
            for (const [key,value] of Object.entries(bins)){
                // console.log(key)
                // console.log(value)
                if (value[0] == "-inf"){
                    if (val[1] < parseFloat(value[1]) && val[1] > -40000){
                        break
                    }
                }
                if (value[1] == "inf"){
                    if (val[1] > parseFloat(value[0]) && val[1] < 4000000){
                        break
                    }
                }
                if (value[0] != "-inf" && value[1] != "inf"){
                    if (val[1] < parseFloat(value[1]) && val[1] > parseFloat(value[0])){
                        break
                    }
                }
                index = index +1
            }
            let bins_names = Object.entries(bins).map(([key,value])=>{return key})
            return {x:index, y:val[0], real_value: val[1], bin_name: bins_names[index]}
        })
        let options = {
            chart: {
                type: 'columnrange',
                height: 125,
                inverted: true,
                // zooming: {
                //     type:"xy"
                // }
                // events: {
                //     load() {
                //       this.showHideFlag = true;
                //     }
                //   }
                
            },
            legend: {
                align: "right",
                layout: 'vertical',
                verticalAlign: 'top',
                width: '10%',
                floating: true,
                itemMarginTop: 5,
                // color: "red"
            },
            accessibility: {
                // point: {
                //     descriptionFormatter: function (point) {
                //         var ix = point.index + 1,
                //             category = point.yCategory,
                //             from = new Date(point.x),
                //             to = new Date(point.x2);
                //         return ix + '. ' + category + ', ' + from.toDateString() +
                //             ' to ' + to.toDateString() + '.';
                //     }
                // }
            },
            plotOptions:{
                series:{
                    events: {
                        legendItemClick: function (){
                            if (this.name === 'See raw data') {

                                this.legendItem.label.styles.color = "#cccccc"
                                //console.log(this.legendItem.label.styles.color)
                                setShow(!show)
                                setColor("Disable raw data")

                            }
                            if (this.name === 'Disable raw data'){
                                setShow(!show)
                                setColor("See raw data")
                            }
                            // chart.showHideFlag = !chart.showHideFlag
                            
                            // console.log(chart.showHideFlag)
                            return false
                        },
                        showCheckbox: true
                    },
                },
                columnrange: {
                    dataLabels: {
                        enabled: false,
                        // format: '{y}°C'
                    },
                    grouping: false,
                    enableMouseTracking: false,  
                },
                arearange:{
                    allowPointSelect: false,
                    enableMouseTracking: false,  
                }
            },
            xAxis: {
                // min: range[0],
                // max: range[1]
                title: {
                    text: name
                },
                categories: Object.entries(bins).map(([key, value])=> {return key}),
                reversed: false
            },
            yAxis: [
            {
                title: {
                    text: ""
                },
                min: range[0],
                max: range[1]
                // ,
                // categories: ["low","high"]
            }],
            title: {
                text: ""
            },
            series: [{id: 'Raw Data',
                name: color,
                type: "scatter",
                data: rawData_divided,
                color: 'black',
                zIndex: 1,
                dataLabels: {
                    enabled: !show,
                    format: '{point.real_value}'
                },
                showInLegend: true,
                inverted: false,
                tooltip: {
                    pointFormat: 'Time: {point.y}<br>Type:{point.bin_name}<br> Value: {point.real_value}',
                }
            },
            ...series]
            }
            return options

        }

    function size_dict(d){
        let c=0; 
        for (let i in d) ++c; 
        return c}
    
    useEffect(() => {
        //
        setRawdata(props.rawData[props.symbol_id])
        setRange(props.range)
        setBins(props.binValues)
        let symbols = []
        let counter = -1
        let range_series = []
        let name = ""
        for (const [key,value] of Object.entries(props.binValues)){
            name = key.split(".")[0]
            break
        }
        if (size_dict(options) != 0){
            let colors = ['#2b908f','#f45b5b','#90ed7d','#f7a35c', '#8085e9']
        for (const [key,value] of Object.entries(props.descriteData)){
            counter+=1
            if (value.length === 0){
                range_series.push({name: key, data: [[counter,1,1]], showInLegend:false, color: colors[counter]})
            }
            else{
            range_series.push({name: key, data: value.map((val) => {
                if (val[0] == props.highlight[0] && val[1] == props.highlight[1]){
                    return {x:counter, low: val[0], high: val[1], selected: true}
                }
                return {x:counter, low: val[0], high: val[1]}}
                ), showInLegend:false, color: colors[counter]})            
            // if (iter[0] == props.highlight[0] && iter[1] == props.highlight[1]){
            //     range_series.pop()
            //     range_series.push({name: key + " [" + parseFloat(bins[key][0]).toFixed(2) + ":" + bins[key][1] + "]",type: 'arearange', data:[[parseInt(iter[0]),parseFloat(bins[key][0]),range[3] + 1],[parseInt(iter[1]),parseFloat(bins[key][0]),range[3]+1]], color:colors[counter], dataLabels: {enabled: false}, showInLegend: false, zIndex: 0, linkedTo: key, lineColor: "#303030", lineWidth: 8})

            // }
        }
        }

        setDescritedata(symbols)
    }
        let series = [
        // ,{
        //     name: 'Intervals',
        //     pointPadding: 0,
        //     groupPadding: 0,
        //     borderColor: 'gray',
        //     pointWidth: 20,
        //     type: 'xrange',
        //     grouping: true,
        //     data: descriteData,
        //     dataLabels: {
        //         enabled: true
        //     }
        // }
        ]
        //console.log(range_series)
        //console.log(options)
        // if (options == {}){
        //     console.log("first timeeeeeeeeeeee")
        //     series.push(...range_series)
        // }
        // else{
        //     range_series = range_series.slice(1)
        //     series.push(...range_series)
        // }
        
        series.push(...range_series)
        let res = createOptions(series,name)
        setOptions(res)

        
        //let colors = ['#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1']
        
        
	}, [rawData,show,color]);

    if (options == []){
        return (
            <CircularProgress style={{ color: 'purple', marginLeft: '45%', marginTop: '20%', width: 75 }}/>
        )
    }
    return (
            <HighchartsReact highcharts={Highcharts} options= {options}></HighchartsReact>
    )
}
export default RawDataGraphNew2;
import React, { Component, useState } from 'react';
import { Container, ToggleButtonGroup, ToggleButton, Col, Row, Button, ButtonGroup, Table} from 'react-bootstrap';

import SearchGraph from './SearchGraph';
import SearchLimitsRelation from './SearchLimitsRelation';
import SearchTable from './SearchTable';
import SearchMeanPresentation from './SearchMeanPresentation';
import PsearchMeanPresentation from './PsearchMeanPresentation';

import TIRPTimeLine from './TIRPTimeLine';
import TIRPsPie from './TIRPsPie';
import DTirpBarPlot from './DTirpBarPlot';
import CustomMultiValue from './CustomMultiValue'
import Select, { components } from "react-select";
import { errorAlert, successAlert } from '../../../SweetAlerts';
import { LinearProgress } from '@material-ui/core';

import {
	getSubTree,
	searchTirps1Class,
	searchTirps2Class,
    searchTirps1ClassRelation
} from '../../../../networking/requests/visualization';

const InputOption = ({
	getStyles,
	Icon,
	isDisabled,
	isFocused,
	isSelected,
	children,
	innerProps,
	...rest
  }) => {
	const [isActive, setIsActive] = useState(false);
	const onMouseDown = () => setIsActive(true);
	const onMouseUp = () => setIsActive(false);
	const onMouseLeave = () => setIsActive(false);
  
	// styles
	let bg = "transparent";
	if (isFocused) bg = "#eee";
	if (isActive) bg = "#B2D4FF";
  
	const style = {
	  alignItems: "center",
	  backgroundColor: bg,
	  color: "inherit",
	  display: "flex "
	};
  
	// prop assignment
	const props = {
	  ...innerProps,
	  onMouseDown,
	  onMouseUp,
	  onMouseLeave,
	  style
	};
  
	return (
	  <components.Option
		{...rest}
		isDisabled={isDisabled}
		isFocused={isFocused}
		isSelected={isSelected}
		getStyles={getStyles}
		innerProps={props}
	  >
		<input type="checkbox" checked={isSelected} style={{opacity:1}}/>
		<div style={{marginLeft:30}}>{children}</div>
	  </components.Option>
	);
  };
const styles = {
	placeholder: (defaultStyles) => {
        return {
            ...defaultStyles,
           display: "contents",
        }
    },

valueContainer: (provided, state) => ({
	whiteSpace: "nowrap",
	// textOverflow: "ellipsis",
	overflow: "hidden",
	flex: "1 0 0%",
	position: "relative",
	border: "solid 0px white",
}),
menu: provided => ({ ...provided, zIndex:2 })
// input: (provided, state) => ({
// 	display: "inline",
// 	border: "solid 1px red"
// })
};
const allen_relation = localStorage.number_of_relations == "7" ? {
	"<": {
		"<": ["<"],
		"m": ["<"],
		"o": ["<"],
		"f": ["<"],
		"c": ["<"],
		"s": ["<"],
		"=": ["<"]
	},
	"m": {
		"<": ["<"],
		"m": ["<"],
		"o": ["<"],
		"f": ["<"],
		"c": ["<"],
		"s": ["m"],
		"=": ["m"]
	},
	"o": {
		"<": ["<"],
		"m": ["<"],
		"o": ["<", "m", "o"],
		"f": ["<", "m", "o"],
		"c": ["<", "m", "o", "f", "c"],
		"s": ["o", "f", "c"],
		"=": ["o"]
	},
	"f": {
		"<": ["<"],
		"m": ["m"],
		"o": ["o"],
		"f": ["f"],
		"c": ["c"],
		"s": ["c"],
		"=": ["f"]
	},
	"c": {
		"<": ["<", "m", "o", "f", "c"],
		"m": ["o","f","c"],
		"o": ["o","f","c"],
		"f": ["c"],
		"c": ["c"],
		"s": ["c"],
		"=": ["c"]
	},
	"s": {
		"<": ["<", "m", "o", "f", "c"],
		"m": ["o","f","c"],
		"o": ["o","f","c"],
		"f": ["c"],
		"c": ["c"],
		"s": ["s"],
		"=": ["s"]
	},
	"=": {
		"<": ["<"],
		"m": ["m"],
		"o": ["o"],
		"f": ["f"],
		"c": ["c"],
		"s": ["s"],
		"=": ["="]
	},
} : {"<": {
	"<": ["<"],
	"c": ["<"],
	"=": ["<"]
},
"c": {
	"<": ["<", "c"],
	"c": ["c"],
	"=": ["c"]
},
"=": {
	"<": ["<"],
	"c": ["c"],
	"=": ["="]
}}
  
class RelationSeach extends Component {
	constructor(props) {
		super(props);

		const tables = JSON.parse(localStorage.States);
		const statesEntries = tables.States.map((state) => {
			const part2 = state.BinLabel ?? state.BinLabel;
			const part1 = state.TemporalPropertyName ?? state.TemporalPropertyID;

			const name = part1 + '.' + part2;

			return [state.StateID, name];
		});

		const statesDict = Object.fromEntries(statesEntries);
		const stateIDs = Object.keys(statesDict);

        const Relation_is_7 = localStorage.number_of_relations == "7" ? {"<":"Before","m":"Meets","o":"Overlaps","f":"Finish-By","c":"Contains","s":"Start-By", "=":"Equal"} : {"<":"Before","c":"Conatins","=":"Equal"}
        console.log(Relation_is_7)
		const relationlist = Object.keys(Relation_is_7)
		this.state = {
			parameters: {
				minSizeCls0: 1,
				maxSizeCls0: 10,
				minHSCls0: 1,
				maxHSCls0: 100,
				minVSCls0: Math.round(parseFloat(localStorage.min_ver_support) * 100),
				maxVSCls0: 100,

				minHSCls1: 1,
				maxHSCls1: 100,
				minVSCls1: Math.round(parseFloat(localStorage.min_ver_support) * 100),
				maxVSCls1: 100,
			},

			minMMD: 0,
			maxMMD: 100,

			startList: stateIDs,
			containList: relationlist,
			endList: stateIDs,
			dictionary_states: statesDict,
            relationDict: Relation_is_7,
			totalNumSymbols: stateIDs.length,

			showGraph: true,
			canExplore: false,
			searchResults: [],
			selected: [],
			measureToRate: {
				vs0: 2,
				vs1: 2,
				mhs0: 2,
				mhs1: 2,
				size: 2,
			},

			allTirps: {},
			chosenTIRP: undefined,
			modalShowRawPop: false,
			mode: 1,
			number_of_symbol: ["First", "Second","Third","Fourth","Fifth"],
			testvalues: [],
			defaultLists:{
				"symbol0": this.createOptions(statesDict)},
			selectedValues:{
				"symbol0": [], 
			},
			relationPosition:[],
			order:[],
			disable:[],
			isSearching: false
			};
		this.getAllTirps()
	}
        
	async getAllTirps() {
		const visualizationId = sessionStorage.getItem('visualizationId');
		const firstLevel = JSON.parse(localStorage.rootElement);
		const subTreesPromises = firstLevel.map(async (tirp) => {
			if (tirp._TIRP__childes.length === 0) return [];

			const data = await getSubTree(tirp._TIRP__symbols[0], visualizationId);
			const tirpsFamily = data['TIRPs'];
			const tirpChildrenArr = tirpsFamily['_TIRP__childes'];
			return tirpChildrenArr;
		});
		const subTressArray = await Promise.all(subTreesPromises);
		const subTress = subTressArray.map((subTree) =>
			this.flatTree(subTree, (tirp) => tirp['_TIRP__childes'])
		);
		const childrenTirps = subTress.flat();

		const allTirps = firstLevel.concat(childrenTirps);
		const allTirpsEntries = allTirps.map((tirp) => [tirp['_TIRP__unique_name'], tirp]);

		this.setState({
			allTirps: Object.fromEntries(allTirpsEntries),
		});
	}

	flatTree(tree, getChildren) {
		return tree.reduce((acc, curr) => {
			const children = getChildren(curr);
			const subTree = this.flatTree(children, getChildren);
			return acc.concat([curr, ...subTree]);
		}, []);
	}

	changeParameter = (event) => {
		const parameterName = event.target.name;
		const parameterRawValue = parseInt(event.target.value);
		// const parameterValue = Math.max(parameterRawValue, event.target.min);
		const parameterValue = Math.max(parameterRawValue, 1);
		const newParameters = { ...this.state.parameters, [parameterName]: parameterValue };
		this.setState({ parameters: newParameters });
	};

	async searchTirps() {
		const visualizationId = sessionStorage.getItem('visualizationId');
		// let flag = false;
        // for (const [key, value] of Object.entries(this.state.selectedLists)) {
		// 	if (value.length == 0){
		// 		this.setState({ searchResults: [] });
		// 		const scrollToElement = document.querySelector('.results-container');
		// 		scrollToElement.scrollIntoView({ behavior: 'smooth' });
		// 		flag = true
		// 		break
		// 	} 
		// }
		// if (flag == true){
		// 	return
		// }
		let empty_lists_symbols = false
		let selectedValues = Object.entries(this.state.selectedValues)
		for (let i = 0 ; i< selectedValues.length; i++){
			selectedValues[i][1] = selectedValues[i][1].map((val)=>{
				if(val.value != undefined){
					return val.value
				}
				else{
					return val
				}
				})
			if (selectedValues[i][1].length == 0){
				empty_lists_symbols = true
			}
				
				
			
		}
		if (empty_lists_symbols == true){
			return
		}
		successAlert(
			'Success',
			'Search initiated, please reamin in the current window to view the results.'
		)
		this.setState({isSearching:true})
		const data = this.props.isPredictive
			? await searchTirps2Class(
					true,
					this.state.startList,
					this.state.containList,
					this.state.endList,
					this.state.parameters.minHSCls0,
					this.state.parameters.maxHSCls0,
					this.state.parameters.minVSCls0,
					this.state.parameters.maxVSCls0,
					this.state.parameters.minHSCls1,
					this.state.parameters.maxHSCls1,
					this.state.parameters.minVSCls1,
					this.state.parameters.maxVSCls1,
					visualizationId,
					this.state.startList.length !== this.state.totalNumSymbols,
					this.state.containList.length !== this.state.totalNumSymbols,
					this.state.endList.length !== this.state.totalNumSymbols
			  )
			: await searchTirps1ClassRelation(
					'',
					selectedValues,
					this.state.parameters.minHSCls0,
					this.state.parameters.maxHSCls0,
					this.state.parameters.minVSCls0,
					this.state.parameters.maxVSCls0,
					visualizationId,
					this.state.startList.length !== this.state.totalNumSymbols,
					this.state.containList.length !== this.state.totalNumSymbols,
					this.state.endList.length !== this.state.totalNumSymbols
			  );
		// const SIZE_IDX = 2;
		this.setState({isSearching:false})
		const searchResults = data['Results'].map((result) => result.split(','));
		// .filter((result)=>(parseInt(result[2]) >= this.state.parameters.minSizeCls0 && parseInt(result[2]) <= this.state.parameters.maxSizeCls0))
		// .filter(
		// 	(result) =>
		// 		parseInt(result[SIZE_IDX]) >= this.state.parameters.minSizeCls0 &&
		// 		parseInt(result[SIZE_IDX]) <= this.state.parameters.maxSizeCls0 &&
		// 		parseInt(result[SIZE_IDX]) >= this.state.parameters.minSizeCls1 &&
		// 		parseInt(result[SIZE_IDX]) <= this.state.parameters.maxSizeCls1
		// );

		this.setState({ searchResults });
		const scrollToElement = document.querySelector('.results-container');
		scrollToElement.scrollIntoView({ behavior: 'smooth' });
	}

	showTableOrGraph = () => {
		const radios = ['Graph', 'Table'];
		return (
			<Col sm={12} className='mb-4'>
				<ToggleButtonGroup defaultValue={0} name='options' style={{ width: '100%' }}>
					{radios.map((radio, idx) => (
						<ToggleButton
							className={'bg-hugobot-toggle-button'}
							key={idx}
							type='radio'
							color='info'
							name='radio'
							value={idx}
							onChange={() => this.setState({ showGraph: radio === 'Graph' })}
						>
							{radio}
						</ToggleButton>
					))}
				</ToggleButtonGroup>
			</Col>
		);
	};

	handleOnSelect(newSelected) {
		const rawSymbols = newSelected[this.props.isPredictive ? 7 : 4];
		const symbols = rawSymbols.slice(1, rawSymbols.length - 1);
		const rawRelations = newSelected[this.props.isPredictive ? 8 : 5];
		const relations = rawRelations.slice(0, rawRelations.length - 1);
		const unique_name = symbols + '|' + relations;

		this.setState({
			selected: newSelected,
			canExplore: true,
			chosenTIRP: this.state.allTirps[unique_name],
		});
	}

	createOptions = (dcit) => {
		let lst = [];
		for (const [key, value] of Object.entries(dcit)) {
			lst.push({label: value, value: key, disabled: false })
		}
		return lst;
	}

	getvalues = (dcit) => {
		let lst = []
		for (var key in dcit) {
			lst.push(dcit[key]);
		  }
		return lst[1]
	}

	renderVisualizationMode = ()=>{
		return (
			<ButtonGroup toggle={true} size='lg' className='w-100 mb-4'>
				<ToggleButton
					checked={this.state.mode == 0}
					className={'btn-hugobot radio-btn-label'}
					onClick={() => {
						this.setState({mode: 0, 
						defaultLists:{
						},
						relationPosition:[],
						order: [],
						disabled: []
					})}}
					type={'radio'}
					value={0}
				>
					Reset
				</ToggleButton>
				<ToggleButton
					checked={this.state.mode == 1}
					className={'btn-hugobot radio-btn-label'}
					onClick={() => {
						this.setState({mode: 1, 
						defaultLists:{
						"symbol0": this.createOptions(this.state.dictionary_states), 
						},
						selectedValues:{
							"symbol0": [], 
						},
						relationPosition:[],
						order: [],
						disable: []
					})}}
					type={'radio'}
					value={0}
					disabled={this.state.mode != 1 && this.state.mode !=0}
				>
					Relation
				</ToggleButton>
				<ToggleButton
					checked={this.state.mode == 2}
					className={'btn-hugobot radio-btn-label'}
					onClick={() => {
						this.setState({mode: 2, 
						defaultLists:{
						"symbol0": this.createOptions(this.state.dictionary_states), 
						"symbol1": this.createOptions(this.state.dictionary_states), 
						"relation1": this.createOptions(this.state.relationDict)},
						selectedValues:{
							"symbol0": [], 
							"symbol1": [], 
							"relation1": []
						},
						relationPosition:[[1]],
						order: [1],
						disable: [true]
					})}}
					type={'radio'}
					value={0}
					disabled={this.state.mode != 2 && this.state.mode !=0}
				>
					2 sized tirp
				</ToggleButton>
				<ToggleButton
					checked={this.state.mode == 3}
					className={'btn-hugobot radio-btn-label'}
					onClick={() => this.setState({mode: 3, 
						defaultLists:{
						"symbol0": this.createOptions(this.state.dictionary_states), 
						"symbol1": this.createOptions(this.state.dictionary_states), 
						"symbol2": this.createOptions(this.state.dictionary_states), 
						"relation1": this.createOptions(this.state.relationDict), 
						"relation2": [], 
						"relation3": this.createOptions(this.state.relationDict)},
						selectedValues:{
							"symbol0": [], 
							"symbol1": [], 
							"symbol2": [], 
							"relation1": [], 
							"relation2": [], 
							"relation3": []
						},
						relationPosition:[[1,2],["",3]],
						order:[1,3,2],
						disable: [true,false,false]
					})}
					type={'radio'}
					value={1}
					disabled={this.state.mode != 3 && this.state.mode !=0}
				>
					3 sized tirp
				</ToggleButton>
				<ToggleButton
					checked={this.state.mode == 4}
					className={'btn-hugobot radio-btn-label'}
					onClick={() => this.setState({mode: 4
						, defaultLists:{
						"symbol0": this.createOptions(this.state.dictionary_states), 
						"symbol1": this.createOptions(this.state.dictionary_states), 
						"symbol2": this.createOptions(this.state.dictionary_states), 
						"symbol3": this.createOptions(this.state.dictionary_states),
						"relation1": this.createOptions(this.state.relationDict), 
						"relation2": [], 
						"relation3": [],
						"relation4": this.createOptions(this.state.relationDict), 
						"relation5": [], 
						"relation6": this.createOptions(this.state.relationDict),
					},
					selectedValues:{
						"symbol0": [], 
						"symbol1": [], 
						"symbol2": [], 
						"symbol3": [],
						"relation1": [], 
						"relation2": [], 
						"relation3": [],
						"relation4": [], 
						"relation5": [], 
						"relation6": [],
					},
					relationPosition:[[1,2,3],["",4,5],["","",6]],
					order:[1,4,2,6,5,3],
					disable: [true,false,false,false,false,false]
					})}
					type={'radio'}
					value={2}
					disabled={this.state.mode != 4 && this.state.mode !=0}
				>
					4 sized tirp
				</ToggleButton>
				<ToggleButton
					checked={this.state.mode == 5}
					className={'btn-hugobot radio-btn-label'}
					onClick={() => this.setState({mode: 5
						, defaultLists:{
							"symbol0": this.createOptions(this.state.dictionary_states), 
							"symbol1": this.createOptions(this.state.dictionary_states), 
							"symbol2": this.createOptions(this.state.dictionary_states), 
							"symbol3": this.createOptions(this.state.dictionary_states),
							"symbol4": this.createOptions(this.state.dictionary_states), 
							"relation1": this.createOptions(this.state.relationDict), 
							"relation2": [], 
							"relation3": [],
							"relation4": [], 
							"relation5": this.createOptions(this.state.relationDict), 
							"relation6": [],
							"relation7": [],
							"relation8": this.createOptions(this.state.relationDict), 
							"relation9": [], 
							"relation10": this.createOptions(this.state.relationDict),
						},
						selectedValues:{
							"symbol0": [], 
							"symbol1": [], 
							"symbol2": [], 
							"symbol3": [],
							"symbol4": [], 
							"relation1": [], 
							"relation2": [], 
							"relation3": [],
							"relation4": [], 
							"relation5": [], 
							"relation6": [],
							"relation7": [],
							"relation8": [], 
							"relation9": [], 
							"relation10": [],
						},
						relationPosition:[[1,2,3,4],["",5,6,7],["","",8,9],["","","",10]],
						order: [1,5,2,8,6,3,10,9,7,4],
						disable: [true,false,false,false,false,false,false,false,false,false]
					})}
					type={'radio'}
					value={3}
					disabled={this.state.mode != 5 && this.state.mode !=0}
				>
					5 sized tirp
				</ToggleButton>
			</ButtonGroup>
		)
	}

	render() {
		const type_of_comp = this.props.isPredictive ? 'disc' : 'tirp';
		return (
			<Container fluid>
				<Row>
					<Col sm={8}>
						{this.state.mode==0? <div style={{height: "100%", alignItems:"center", display: "flex", justifyContent: "center", fontSize: "50px"}}>Choose tirp size</div>:<></>}
								<Table
								striped={true}
								bordered={true}
								style={{ tableLayout: 'fixed', textAlign: 'center' }}
								>
									<thead>
										<tr>{Array.from({length: this.state.mode}, () => 0).map((k,index)=>{
											if (index!= 0 || this.state.mode == 1){
											return(<td><div style={{height: "50px"}}>
											{/* top row */}
									<Select
										styles={styles}
										isSearchable
										isMulti
										placeholder={this.state.mode != 1 ? "Select " + this.state.number_of_symbol[index]+" symbol" : "Select desired symbols to view their relations."}
										closeMenuOnSelect={false}
										hideSelectedOptions={false}
										getOptionValue={(option) => option["value"]}
										options={[{label:"All", value:""},...this.state.defaultLists["symbol" + index.toString()]]}
										onChange={((options) => {
											let symbols_list = []
											options.length &&
											options.find(option => option.value === "")
												? symbols_list = [...this.createOptions(this.state.dictionary_states).map((obj)=>{return obj.value})]
												: symbols_list = options.map((obj) => {return obj.value})	
											console.log(index)
											let originalValue = {...this.state.selectedValues,  ["symbol" + (index).toString()]: symbols_list.map((val)=>{return {label:this.state.dictionary_states[val], value: val, disabled:false}})}
											this.setState({selectedValues: originalValue})
											console.log(originalValue)
										})}
										components={{
										MultiValue: CustomMultiValue,
										Option: InputOption
										}}
									/>
									</div></td>)}
											else{return(<td></td>)}
											})}</tr>
									</thead>
									<tbody>
										{Array.from({length: this.state.mode-1}, () => 0).map((i, index_row)=>{
											return(<tr>{Array.from({length: this.state.mode}, () => 0).map((j, index_col)=>{
												if (index_col== 0){
													return(<td><div style={{height: "50px", width:"100%"}}>
														{/* first column */}
													<Select
														styles={styles}
														isSearchable
														isMulti
														placeholder={"Select " + this.state.number_of_symbol[index_row]+" symbol"}
														closeMenuOnSelect={false}
														hideSelectedOptions={false}
														isDisabled={index_row!=0}
														getOptionValue={(option) => option["value"]}
														options={[{label:"All", value:""},...this.state.defaultLists["symbol" + index_row.toString()]]}
														onChange={((options) => {
																let symbols_list = []
																options.length &&
																options.find(option => option.value === "")
																	? symbols_list = [...this.createOptions(this.state.dictionary_states).map((obj)=>{return obj.value})]
																	: symbols_list = options.map((obj) => {return obj.value})	
																let originalValue = {...this.state.selectedValues,  ["symbol" + (index_row).toString()]: symbols_list.map((val)=>{return {label:this.state.dictionary_states[val], value: val, disabled:false}})}
																this.setState({selectedValues: originalValue})
																console.log(originalValue)
														})}
														components={{
														MultiValue: CustomMultiValue,
														Option: InputOption
														}}
													/>
													</div></td>)
												}
												else{
													if(index_col - index_row >= 1){
														return(<td><div style={{height: "40px"}}>
															{/* relation */}
															{console.log(this.state.disable)}
											{console.log(this.state.defaultLists["relation" + (this.state.relationPosition[index_row][index_col-1]).toString()])}
													<Select
														styles={styles}
														isSearchable
														isMulti
														isDisabled={this.state.disable[this.state.order.findIndex(dis => dis == this.state.relationPosition[index_row][index_col-1])] == false}
														placeholder={"Select relation"}
														closeMenuOnSelect={false}
														hideSelectedOptions={false}
														getOptionValue={(option) => option["value"]}
														options={[{label:"All", value:""},...this.state.defaultLists["relation" + (this.state.relationPosition[index_row][index_col-1]).toString()]]}
														onChange={((options) => {
																let selectedList = {...this.state.selectedValues, ["relation" + (this.state.relationPosition[index_row][index_col-1]).toString()]: options.map((obj) => {return obj.value})}
																let originalValue = {...this.state.selectedValues,  ["relation" + (this.state.relationPosition[index_row][index_col-1]).toString()]: this.state.defaultLists["relation" + (this.state.relationPosition[index_row][index_col-1]).toString()].map((obj)=>{return obj.value})}
																options.length &&
																options.find(option => option.value === "")
																	? this.setState({ selectedValues: originalValue })
																	: this.setState({ selectedValues: selectedList});
																console.log("after")
																let next_index = this.state.order.findIndex(dis => dis == this.state.relationPosition[index_row][index_col-1])
																let new_disable_list=[]
																for (let p = 0; p<this.state.order.length; p++){
																	if (p<this.state.order.length){
																		if (p<=next_index){
																			new_disable_list.push(true)
																		}
																		else{
																			new_disable_list.push(false)
																		}
																	}
																	else{
																		break
																	}
																}
																this.setState({disable:new_disable_list})
																// if (options.length==0){
																// 	return
																// }
																new_disable_list[next_index+1] = true
																	this.setState({disable:new_disable_list})
																if (index_row != 0){
																	let find_most_left_at_above_row_index = 0
																	for (let k=0; k< this.state.relationPosition[index_row-1].length; k++){
																		if (this.state.relationPosition[index_row-1][k]==""){
																			continue
																		}
																		else{
																			find_most_left_at_above_row_index = k
																			break
																		}
																	}
																	let values_of_most_left = this.state.selectedValues["relation" + (this.state.relationPosition[index_row-1][find_most_left_at_above_row_index]).toString()]
																	let possible_values = []
																	let all_list = []
																	options.find(option => option.value === "")
																	? all_list= originalValue["relation" + (this.state.relationPosition[index_row][index_col-1]).toString()]
																	: all_list= options.map((value)=>{return value.value});
																	for (let ab = 0; values_of_most_left.length; ab++){
																		if (ab<values_of_most_left.length){
																			for (let bc=0; all_list.length; bc++){
																				if (bc<all_list.length){
																					if (all_list[bc] != ""){
																						
																						let possible_options = allen_relation[values_of_most_left[ab]][all_list[bc]]
																						for (let value=0; possible_options.length; value++){
																							if (value<possible_options.length){
																								if (!possible_values.includes(possible_options[value])){
																									possible_values.push(possible_options[value])
																								}
																							}
																							else{
																								break
																							}
																							
																						}
																					}
																				}
																				else{
																					break
																				}
																			}
																		}
																		else{
																			break
																		}
																		
																	}
																	let originalValue2 = {...this.state.defaultLists,  ["relation" + (this.state.relationPosition[index_row-1][index_col-1]).toString()]: possible_values.map((val)=>{return {label:this.state.relationDict[val], value: val, disabled:false}})}
																	this.setState({
																		defaultLists: originalValue2
																	})
																}
														})}
														components={{
														MultiValue: CustomMultiValue,
														Option: InputOption
														}}
													/>
													</div></td>)
													}
													return(<td></td>)}
												})}</tr>)}
											)
										}
									</tbody>
								</Table>
						{/* <Row style={{ position: 'absolute', height: '100%' }}>
							<Col sm={4} style={{ height: '100%' }}>
								<SearchIntervals
									title='First'
									intervals={this.state.dictionary_states}
									changeList={(startList) => this.setState({ startList })}
                                    relation={false}
								/>
							</Col>
							<Col sm={4} style={{ height: '100%' }}>
								<SearchIntervals
									title='Relation'
									intervals={this.state.relationDict}
									changeList={(containList) => this.setState({ containList })}
                                    relation={true}
								/>
							</Col>
							<Col sm={4} style={{ height: '100%' }}>
								<SearchIntervals
									title='Last'
									intervals={this.state.dictionary_states}
									changeList={(endList) => this.setState({ endList })}
                                    relation={false}
								/>
							</Col>
						</Row> */}
					</Col>
					<Col sm={4}>
						{this.renderVisualizationMode()}
						<SearchLimitsRelation
							searchTirps={this.searchTirps.bind(this)}
							measureToRate={this.state.measureToRate}
							changeMeasureToRate={(measureToRate) =>
								this.setState({ measureToRate })
							}
							parameters={this.state.parameters}
							changeParameter={this.changeParameter}
							isPredictive={this.props.isPredictive}
						/>
						{this.state.isSearching == true ? <>
									<figure class="highcharts-figure">
										<LinearProgress/>
									</figure>
								</>: <></>}
					</Col>
				</Row>
				<Row className='results-container'>
					<Col sm={8}>
						{this.showTableOrGraph()}
						{this.state.showGraph ? (
							<SearchGraph
								isPredictive={this.props.isPredictive}
								minVS0={this.state.parameters.minVSCls0}
								maxVS0={this.state.parameters.maxVSCls0}
								minHS0={this.state.parameters.minHSCls0}
								maxHS0={this.state.parameters.maxHSCls0}
								minSize0={this.state.parameters.minSizeCls0}
								maxSize0={this.state.parameters.maxSizeCls0}
								minVS1={this.state.parameters.minVSCls1}
								maxVS1={this.state.parameters.maxVSCls1}
								minHS1={this.state.parameters.minHSCls1}
								maxHS1={this.state.parameters.maxHSCls1}
								handleOnSelect={this.handleOnSelect.bind(this)}
								measureToRate={this.state.measureToRate}
								selectedSymbols={
									this.state.selected[this.props.isPredictive ? 7 : 4]
								}
								selectedRelations={
									this.state.selected[this.props.isPredictive ? 8 : 5]
								}
								tirps={this.state.searchResults}
								dictionary_states={this.state.dictionary_states}
							/>
						) : (
							<SearchTable
								handleOnSelect={this.handleOnSelect.bind(this)}
								isPredictive={this.props.isPredictive}
								tirps={this.state.searchResults}
							/>
						)}
					</Col>
					<Col sm={4}>
						<Row>
							<Col sm={1}></Col>
							<Col sm={11}>
								{this.props.isPredictive ? (
									<PsearchMeanPresentation
										canExplore={this.state.canExplore}
										vs1={this.state.selected[1]}
										vs0={this.state.selected[0]}
										mmd1={this.state.selected[4]}
										mmd0={this.state.selected[5]}
										mhs1={this.state.selected[2]}
										mhs0={this.state.selected[3]}
										currentLevel={this.state.selected[6]}
										symbols={this.state.selected[7]}
										relations={this.state.selected[8]}
									/>
								) : (
									<SearchMeanPresentation
										canExplore={this.state.canExplore}
										vs={this.state.selected[0]}
										mhs={this.state.selected[1]}
										mmd={this.state.selected[2]}
										currentLevel={this.state.selected[3]}
										symbols={this.state.selected[4]}
										relations={this.state.selected[5]}
										row={this.state.selected}
									/>
								)}
							</Col>
						</Row>
					</Col>
				</Row>
				{this.state.chosenTIRP && (
					<Row>
						<Col sm={4}>
							<TIRPsPie row={this.state.chosenTIRP} type_of_comp={type_of_comp} />
						</Col>
						{this.props.isPredictive && (
							<Col lg={3}>
								<DTirpBarPlot row={this.state.chosenTIRP} />
							</Col>
						)}
						<Col sm={this.props.isPredictive ? 5 : 8}>
							<TIRPTimeLine
								tirp={this.state.chosenTIRP}
								type_of_comp={type_of_comp}
							/>
						</Col>
					</Row>
				)}
			</Container>
		);
	}
}

export default RelationSeach;

import React, { Component } from 'react';
import Cookies from 'js-cookie';

import { Card, Form, Table } from 'react-bootstrap';
import { getAllVisualizations } from '../../../networking/requests/datasetsStats';

import VisualizationRow from './VisualizationRow';
import { errorAlert } from '../../SweetAlerts';
import Loader from '../../Loader';
import { SetTitleContext } from '../../../contexts';
/**
 * this class contains the display of the the table from table content
 */

class HomeTable extends Component {
	static contextType = SetTitleContext;
	state = {
		filterDatasetName: '',
		filterCategory: '',
		filterSize: '',
		filterOwner: '',
		filterPublicPrivate: '',
		rows: null,
	};

	filter = () => {
		this.setState({
			filterCategory: document.getElementById('category').value,
			filterDatasetName: document.getElementById('datasetName').value,
			filterOwner: document.getElementById('owner').value,
			filterPublicPrivate: document.getElementById('permission').value,
		});
		this.forceUpdate();
	};

	componentDidMount() {
		if (!Cookies.get('auth-token')) {
			window.open('#/Login', '_self');
		}

		getAllVisualizations()
			.then((data) => {
				this.setState({ rows: data.DataSets });
			})
			.catch(errorAlert);
	}

	renderTableHeader = () => {
		return (
			<thead style={{position: "sticky", top: 0, zIndex: 1}}>
				<tr>
					<th style={{ position: "sticky", top: 0, zIndex: 1 , background:"#FFFFFF"}}>
						<Form.Control
							className={'font-weight-bold'}
							id={'datasetName'}
							onChange={this.filter}
							placeholder={'Dataset Name'}
							type={'text'}
						/>
					</th>
					<th style={{ position: "sticky", top: 0, zIndex: 1 , background:"#FFFFFF"}}>
						<Form.Control
							className={'font-weight-bold'}
							id={'category'}
							onChange={this.filter}
							placeholder={'Category'}
							type={'text'}
						/>
					</th>
					<th style={{ position: "sticky", top: 0, zIndex: 1 , background:"#FFFFFF"}}>
						<Form.Control
							className={'font-weight-bold'}
							id={'owner'}
							onChange={this.filter}
							placeholder={'Owner'}
							type={'text'}
						/>
					</th>
				</tr>
			</thead>
		);
	};

	renderTableData = () => {
		return this.state.rows.map((iter, idx) => (
			<VisualizationRow
				StartVisualization={this.StartVisualization}
				visualization={iter}
				key={idx}
				removeVisualization={(visualizationID) => {
					this.setState((state) => ({
						rows: state.rows.filter(
							(visualization) => visualization.id !== visualizationID
						),
					}));
				}}
			/>
		));
	};

	onClick = () => {
		window.open('#/Upload/Metadata', '_self');
	};

	StartVisualization = (id, visualizationId, discretization, karmalego) => {
		sessionStorage.setItem('datasetReadyName', id);
		sessionStorage.setItem('visualizationId', visualizationId);
		sessionStorage.setItem('discretization1', discretization.method);
		sessionStorage.setItem('discretization2', discretization.paa);
		sessionStorage.setItem('discretization3', discretization.number_of_bins);
		sessionStorage.setItem('discretization4', discretization.interpolation_gap);
		sessionStorage.setItem('karmalego1', karmalego.min_ver_support * 100);
		sessionStorage.setItem('karmalego2', karmalego.max_gap);
		sessionStorage.setItem('karmalego3', karmalego.num_relations);
		sessionStorage.setItem('karmalego4', karmalego.epsilon);
		sessionStorage.setItem('karmalego5', karmalego.max_tirp_length);
		sessionStorage.setItem('karmalego6', karmalego.index_same);
		this.context(id);
		window.open('#/TirpsApp', '_self');
	};

	render() {
		return (
			<Card>
				<Card.Header className={'bg-hugobot'}>
					<Card.Text className={'h3 text-hugobot'}>Visualization DataSets</Card.Text>
				</Card.Header>
				<Card.Body style={{overflow: "auto", height: "80vh",position: "sticky", top: 0, zIndex: 1, paddingTop: 0, paddingLeft: "1.25rem"}}>
					<Table striped={true} bordered={true} hover={true}>
						{this.renderTableHeader()}
						<tbody>
							{this.state.rows ? (
								this.renderTableData()
							) : (
								<tr>
									<td colSpan='5' className='w-100 align-loader'>
										<Loader />
									</td>
								</tr>
							)}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		);
	}
}

export default HomeTable;
